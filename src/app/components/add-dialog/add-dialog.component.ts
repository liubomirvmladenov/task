import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductModel } from '../../models/product.interface';
import { DialogType } from '../../models/dialog-type.enum';
import { ContactModel } from '../../models/contact.interface';

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss']
})
export class AddDialogComponent implements OnInit {
  public group: any = {};
  public formGroup: FormGroup;
  public model = [];

  constructor(
    public dialogRef: MatDialogRef<AddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogType
  ) {}
  
  public ngOnInit(): void {
    this.formGroup = this.getGroup();
  }

  public onCancel(): void {
    this.dialogRef.close();
  }

  public onAdd(): void {}

  public getGroup(): FormGroup {
    if (this.data === DialogType.Product) {
      Object.keys(ProductModel).forEach(key => {
        this.model.push(key);
        this.group[key] = new FormControl('');
      });
    } else {
      Object.keys(ContactModel).forEach(key => {
        this.model.push(key);
        this.group[key] = new FormControl('');
      });
    }

    return new FormGroup(this.group);
  }
}
