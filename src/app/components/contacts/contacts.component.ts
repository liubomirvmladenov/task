import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { DialogType } from '../../models/dialog-type.enum';
import { IPageParams, ISortParams } from '../../models/query-params.interface';
import { ContactsService } from '../../services/contacts.service';
import { AddDialogComponent } from '../add-dialog/add-dialog.component';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  private pageParams: IPageParams = {page: 1, pageSize: 20};
  private sortParams: ISortParams;

  public readonly displayedColumns: string[] = ['name', 'type'];
 
  public dataSource = new MatTableDataSource([]);
  public pageTotal: number = 0;
  public isLoadingResults: boolean;

  constructor(private contactsService: ContactsService, public dialog: MatDialog) { }

  public ngOnInit(): void {
    this.getContacts(1);
  }

  public applyFilter(ev: KeyboardEvent): void {
    const filterValue = (ev.target as HTMLInputElement).value;
  }

  public pageEv(ev): void {
    this.getContacts(ev.pageIndex + 1);
  }

  public getContacts(index: number): void {
    this.isLoadingResults = true;
    this.pageParams.page = index;
    let requestParams;
    if (this.sortParams?.sortDirection) {
      requestParams = {...this.pageParams, ...this.sortParams };
    } else {
      requestParams = this.pageParams;
    }
    this.contactsService.getContacts(requestParams).then((res: any) => {
      this.isLoadingResults = false;
      this.pageTotal = res.meta.paging.total;
      this.dataSource =  new MatTableDataSource(res.contacts)
    });
  }

  public onSortChange(ev) {
    this.sortParams = {sortProperty: ev.active, sortDirection: ev.direction.toUpperCase()};
    this.getContacts(1)
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      width: '350px',
      data: DialogType.Contact
    });
  }

}
