import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { MatTableDataSource } from '@angular/material/table';
import { AddDialogComponent } from '../add-dialog/add-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { DialogType } from '../../models/dialog-type.enum';
import { IPageParams, ISortParams } from '../../models/query-params.interface';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  private pageParams: IPageParams = {page: 1, pageSize: 20};
  private sortParams: ISortParams;

  public readonly displayedColumns: string[] = ['name', 'description'];
  
  public dataSource = new MatTableDataSource([]);
  public pageTotal: number = 0;
  public isLoadingResults: boolean;

  constructor(private produtsService: ProductsService, public dialog: MatDialog) { }

  public ngOnInit(): void {
    this.getProducts(1);
  }

  public applyFilter(ev: KeyboardEvent): void {
    const filterValue = (ev.target as HTMLInputElement).value;
  }

  public pageEv(ev): void {
    this.getProducts(ev.pageIndex + 1);
  }

  public getProducts(index: number): void {
    this.isLoadingResults = true;
    this.pageParams.page = index;
    let requestParams;
    if (this.sortParams?.sortDirection) {
      requestParams = {...this.pageParams, ...this.sortParams };
    } else {
      requestParams = this.pageParams;
    }
    this.produtsService.getProducts(requestParams).then((res: any) => {
      this.isLoadingResults = false;
      this.pageTotal = res.meta.paging.total;
      this.dataSource =  new MatTableDataSource(res.products)
    });
  }

  public onSortChange(ev) {
    this.sortParams = {sortProperty: ev.active, sortDirection: ev.direction.toUpperCase()};
    this.getProducts(1)
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      width: '350px',
      data: DialogType.Product
    });
  }

}
