export const ProductModel = {
  organization: '',
  name: '',
  description: '',
  account: '',
  productNo: '',
  suppliersProductNo: '',
  salesTaxRuleset: '',
  isArchived: false,
  prices: ''
}