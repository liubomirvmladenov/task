export interface IPageParams {
  page: number;
  pageSize: number;
}

export interface ISortParams {
  sortProperty?: string;
  sortDirection?: string;
}
