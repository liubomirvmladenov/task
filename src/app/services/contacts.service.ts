import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  constructor(private httpService: HttpService) { }

  public getContacts(params): Promise<Object> {
    const path = '/contacts';

    return this.httpService.get(path, params);
  }

  public createContact() {}
}
