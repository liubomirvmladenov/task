import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  public get(path: string, params?: any) {
    const url = environment.apiURI + path;
    const options = {headers: this.getHeaders(), params: params};

    return this.http.get(url, options).toPromise();
  }

  public post(path: string, body: any) {
    const url = environment.apiURI + path;
    const options = {headers: this.getHeaders()};

    return this.http.post(url, body, options).toPromise();
  }

  private getHeaders(): HttpHeaders {
    return new HttpHeaders().set('X-Access-Token',  environment.apiToken);
  }
}
