import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private httpService: HttpService) { }

  public getProducts(params): Promise<Object> {
    const path = '/products';

    return this.httpService.get(path, params);
  }

  public createProduct() {}
}
